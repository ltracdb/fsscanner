from fsscanner.scanner import BaseFileInterpreter


class DebugFileInterpreter():

    def interpreter(self, meta):
        if type(meta) != dict:
            raise TypeError("Require dict of handlers. Got " + str(type(meta)))
        meta['interpretered'] = True
        return meta

    def mimes():
        return ['']
