import pymongo
import time
import logging
from fsscanner.scanner import BaseFileCache


logger = logging.getLogger(__name__)


class MongoDbFileCache(BaseFileCache):
    '''
    An implementation of BaseFileCache for MongoDB backend.
    @todo not sure it {'path': {'$regex': '^' + path}} uses index. Probably not.
    '''
    _client = None
    _db = None
    _files = None

    def __init__(self, conn, dbName, collectionName):
        ''' Initialize MongoDB connection. '''
        self._client = pymongo.MongoClient(conn, 27017)
        self._db = self._client[dbName]
        self._files = self._db[collectionName]
        self._files.ensure_index([('path', pymongo.ASCENDING)], unique=True)

    def findOne(self, path):
        ''' @override '''
        self.validatePath(path)
        return self._files.find_one({'path': path})

    def insertOne(self, record):
        ''' @override '''
        self.validatePath(record['path'])
        logger.debug('Insert ' + str(record))
        record['touch'] = time.time()
        record['is_deleted'] = False
        self._files.replace_one({'path': record['path']}, record, upsert=True)

    def markDeleted(self, timeSince, path='/'):
        ''' @override '''
        self.validatePath(path)
        result = self._files.update_many(
            {'path': {'$regex': '^' + path}, 'touch': {'$lt': timeSince}},
            {'$set': {'is_deleted': True}}
        )
        logger.debug('Updated ' + str(result.modified_count))

    def purgeDeleted(self, path='/'):
        ''' @override '''
        self.validatePath(path)
        result = self._files.delete_many({'path': {'$regex': '^' + path}, 'is_deleted': True})
        logger.debug('Purged ' + str(result.deleted_count))

    def drop(self, path='/'):
        ''' @override '''
        self.validatePath(path)
        result = self._files.delete_many({'path': {'$regex': '^' + path}})
        logger.debug('Dropped ' + str(result.deleted_count))
