from setuptools import setup, find_packages
import sys
import os


version = '0.2'
setup(
    name='fsscanner',
    version=version,
    description="",
    classifiers=[
        'Operating System :: POSIX :: Linux',
        'Programming Language :: Python :: 3.4',
    ],  # Get strings from http://pypi.python.org/pypi?%3Aaction=list_classifiers
    keywords='',
    author='Sam Pinkus',
    author_email='sgpin1@student.monash.edu',
    license='',
    packages=['fsscanner'],
    package_dir={'fsscanner': '.'},
    include_package_data=True,
    zip_safe=False,
    install_requires=[],
)
