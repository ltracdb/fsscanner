import sys
import os
import logging
from pprint import pprint
from fsscanner.scanner import Scanner, statify, _mimeMatch
from fsscanner.debugfilecache import DebugFileCache
from fsscanner.debugfileinterpreter import DebugFileInterpreter


logging.basicConfig()
os.chdir(os.path.dirname(__file__))
cache = DebugFileCache()
interpreter = DebugFileInterpreter()
scanner = Scanner(cache, {'': interpreter})
scanner.scanDir('../tests/testdata/')
for path in cache._cache.keys():
    print(path)
