import sys
import os
from os.path import dirname
from pprint import pprint
from time import time
import logging
from fsscanner.scanner import Scanner
from mongodbfilecache import MongoDbFileCache


logging.getLogger().setLevel(logging.DEBUG)
logging.basicConfig()
logging.getLogger().setLevel(logging.DEBUG)
cache = MongoDbFileCache('mongodb://test:XXX@localhost/ltrac', 'ltrac', 'files')
scanner = Scanner(cache)
scanner.scanDir(dirname(__file__) + '/../tests/testdata')
cache.purgeDeleted()
cache.drop()
