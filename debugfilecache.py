from os.path import *
import logging
from fsscanner.scanner import BaseFileCache


logger = logging.getLogger(__name__)


class DebugFileCache(BaseFileCache):
    ''' This is a mock FileCache that stores records in an in memory dict. '''
    _cache = {}

    def __init__(self):
        pass

    def findOne(self, path):
        record = None
        try:
            record = self._cache['path']
        except KeyError:
            pass
        return record

    def insertOne(self, record):
        logger.debug('insertOne()')
        self.validatePath(record['path'])
        if type(record) != dict:
            raise TypeError("Expected dict. Got " + str(type(record)))
        if record['path'] not in self._cache:
            self._cache[record['path']] = record
        else:
            self._cache[record['path']].update(record)

    def markDeleted(self, timeSince, path='/'):
        logger.debug('markDeleted() ' + str(timeSince) + ", " + path)
        self.validatePath(path)
        for _path in list(self._cache.keys()):
            if _path.startswith(path) and self._cache[_path]['touch'] < timeSince:
                self._cache[_path]['is_deleted'] = True
            else:
                pass

    def purgeDeleted(self, path='/'):
        logger.debug('purgeDeleted()')
        self.validatePath(path)
        for _path in list(self._cache.keys()):
            if _path.startswith(path) and self._cache[_path]['is_deleted']:
                del(self._cache[_path])

    def drop(self, path='/'):
        logger.debug('drop()')
        self.validatePath(path)
        for _path in list(self._cache.keys()):
            if _path.startswith(path):
                del(self._cache[_path])
