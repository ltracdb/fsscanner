#!/usr/bin/env python3
''' Provides a filesystem scanner. '''
import sys
import os
import time
import re
import glob
import mimetypes
import uuid
import logging


logger = logging.getLogger(__name__)


class Scanner():
    '''
    Scans a filesystem for files, builds meta data records and stuffs them into a cache.
    '''
    _cache = None
    _interpreters = None
    _absolutePaths = False

    def __init__(self, cache, interpreters={}, absolutePaths=False):
        '''
        Initialize Scanner. `cache` is a cache/db to stuff found file records into.
        Optionally a dict of MIME pattern to interpreter can be passed in `interpreters`.
        These will be passed file records matching the MIME pattern. They can add arbitrary data to the record.
        If absolutePaths use the abspath as the path otherwise all paths are rebased to rootPath in scanDir().
        '''
        if not isinstance(cache, BaseFileCache):
            raise TypeError("Require BaseFileCache. Got " + str(type(cache)))
        if not type(interpreters) == dict:
            raise TypeError("Require dict of interpreters. Got " + str(type(interpreters)))
        self._cache = cache
        self._interpreters = interpreters
        self._absolutePaths = absolutePaths
        # Init mimes. For some reason the Python module does not know about these but `file` does.
        mimetypes.init()
        mimetypes.add_type('application/x-hdf', '.h5')
        mimetypes.add_type('application/x-hdf', '.hdf5')

    def scanDir(self, rootPath, maxDepth=10, markUnScannedDeleted=False):
        '''
        Do a recursive _scanDir(). When finished scan mark any records no present under rootPath
        as deleted if not already.
        If you actually want to delete files you need to call purgeDeleted() on the cache after scanDir().
        `markUnScannedDeleted` will mark all files not touch in this scan for deletion even if
        they're not under rootPath.
        markUnScannedDeleted has no effect when self.absolutePaths=False because rootPath becomes
        equivalent to the actual root.
        '''
        mark = time.time()
        if type(rootPath) != str:
            raise TypeError("Require str. Got " + str(type(rootPath)))
        rootPath = os.path.abspath(rootPath)  # Ensure resolved.
        self._scanDir(rootPath, rootPath, maxDepth)
        if markUnScannedDeleted or not self._absolutePaths:
            rootPath = '/'
        self._cache.markDeleted(mark, rootPath)

    def _scanDir(self, rootPath, prefixPath, maxDepth=10, depth=0):
        '''
        Make records of all files. Do not follow symlinks. Ignore symlinks by default.
        If a record is made for a dir => record made for all files that aren't dir under the dir.
        '''
        if depth <= 3:
            logger.info('Scanning %s%s' % (depth*'  ', rootPath))
        if type(rootPath) != str:
            raise TypeError("Require str. Got " + str(type(rootPath)))
        rootPath = os.path.abspath(rootPath)  # Ensure resolved.
        if depth <= maxDepth:
            logger.debug((' ' * depth) + str(rootPath))
            self.insert(rootPath, prefixPath)
            for path in os.listdir(rootPath):
                path = rootPath + '/' + path
                logger.debug((' ' * depth) + str(path))
                if not os.path.exists(path):
                    continue
                if self.skipPath(path):
                    logger.debug((' ' * depth) + 'Skip ' + str(path))
                    continue
                if os.path.isdir(path):
                    logger.debug((' ' * depth) + 'Scan dir ' + str(path))
                    self._scanDir(path, prefixPath, maxDepth, depth+1)
                elif os.path.isfile(path):
                    self.insert(path, prefixPath)
        else:
            logger.info((' ' * depth) + "Reached depth. Not following " + str(rootPath))

    def insert(self, path, prefixPath):
        ''' Build the update or insert record into cache. '''
        keyPath = path
        if not self._absolutePaths:
            keyPath = re.sub(prefixPath + '/?', '/', path)
        record = statify(path)
        oldRecord = self._cache.findOne(keyPath)
        if oldRecord is None or oldRecord['mtime'] < record['mtime']:
            if oldRecord is None:
                logger.debug("New record: " + keyPath)
                record['uuid'] = str(uuid.uuid4())
            else:
                logger.debug("Update record: " + path)
                oldRecord.update(record)
                record = oldRecord
            self._dispatchInterpreters(record)
            record['path'] = keyPath
            record['dirname'] = str(os.path.dirname(keyPath))
        else:
            logger.debug("Unchanged record: " + path)
            record = oldRecord
        record['touch'] = time.time()  # Have to always touch. @todo ineff
        record['is_deleted'] = False
        self._cache.insertOne(record)

    def _dispatchInterpreters(self, record):
        '''
        Add type specific meta data to file record by dispatching to type specific interpreters.
        There is many ways to arrange this dispatch. For now:
            - interpreters must specify a MIME pattern as a 2-tuple.
            - The first interpreter matching is invoked. Only the first is called then we break.
            - Example MIME patterns 'image/png', 'image', ''.
            - Example invalid MIME patterns 'image/*', 'image/', '*/*'.
        It is up to the client to provide interpreters in appropriate order.
        The interpreter can return any type, but it should be JSON encodable. The return value is storde in meta.
        The interpreter takes the entire record but it should not add,rm,modify fields.
        If the interpreter raises, try to keep going. We should not expect the interpreters not to blow up.
        '''
        for mime, filt in self._interpreters.items():
            if _mimeMatch(record['mime'], mime):
                try:
                    meta = filt.interpreter(record)
                    if meta is not None:
                        record['meta'] = meta
                        break
                except (Exception) as e:
                    logger.exception(e)

    def skipPath(self, path):
        if os.path.islink(path):
            return True
        if os.path.basename(path).startswith('.'):
            return True
        return False


class BaseFileCache():
    '''
    "Abstract" base class for a cache of file meta data. Provides a simple consistent API to storage backends.
    Supports insertion of a single file record, deletion, and drop of entire file sub trees.
    Deleting is disctinct from dropping and purging. Deleting merely marks file deleted.
    All paths should be prefixed with "/". This does not meaning the paths are absolute filesystem paths.
    It mean all paths are absolute within the given cache. How that maps to actual filesystem
    is not the responsibilty of this interface.
    @todo actually use Python BS ABC stuff.
    '''

    def __init__(*args, **kwargs):
        pass

    def findOne(self, path):
        ''' Find a record by path. Return a dict or None if no such a record. '''
        pass

    def insertOne(self, record):
        ''' Insert a single path (file or directory). If the record exists update it. '''
        pass

    def markDeleted(self, timeSince, path='/'):
        '''
        Mark all files under path as deleted if they have not need touched since timeSince.
        Only make sense to call this directly after inserting a batch of records.
        '''
        pass

    def purgeDeleted(self, path='/'):
        ''' Purge (delete) all records marked as deleted under path. '''
        pass

    def drop(self, path='/'):
        ''' Drop all files with the same suffux as `path`. '''
        pass

    def validatePath(self, path):
        if(str(path)[0] != '/'):
            raise InvalidFileNameException("Invalid path '" + path + ". Paths must begin with '/'")


class BaseFileInterpreter():
    '''
    "Abstract" base class for file interpreter. A interpreter essentially provides the interpreter() method.
    @see interpreter()
    @todo actually use Python BS ABC stuff.
    '''

    def __init__(*args, **kwargs):
        pass

    def interpreter(self, record):
        '''
        Interprete the file record in `record`. `record` is expected to be of type dict and have at least a `path` key.
        `path` is the path to the file.
        Should return None or a type that can be encoded with json.dumps()
        '''
        if type(record) != dict:
            raise TypeError("Require dict of handlers. Got " + str(type(record)))
        return None

    def mimes():
        '''
        Return a list of file MIMEs this fileter can handle.
        There is 3 class of MIME matches: '' (anything), 'type' (type/*), 'type/subtype' (type/subtype)
        '''
        return ['']


def statify(path):
    '''
    Build basic file stats.
    Note Pythons MIME library returns (None, None) for directory. Linux: "inode/directory", PHP: "directory".
    I'm forcing it to be consistent with Linux.
    '''
    meta = {}
    stat = os.stat(path)
    meta['path'] = path
    meta['dirname'] = str(os.path.dirname(path))
    meta['name'] = str(os.path.basename(path))
    meta['size'] = stat.st_size
    meta['atime'] = round(stat.st_atime, 3)  # floats. 3 dp ensures no loss of prec in storage.
    meta['ctime'] = round(stat.st_ctime, 3)
    meta['mtime'] = round(stat.st_mtime, 3)
    meta['type'] = 'directory' if os.path.isdir(path) else ('file' if os.path.isfile(path) else '')
    meta['mime'] = mimetypes.guess_type(str(path))[0]
    # Force dir MIME, and such that mime is always a string.
    if meta['mime'] is None:
        if meta['type'] == 'directory':
            meta['mime'] = 'inode/directory'
        else:
            meta['mime'] = ''
    return meta


def _mimeMatch(targetMime, matchMime):
    '''
    See if `matchMime` matches `targetMime`. `targetMime` is expected to be a full MIME or empty string.
    `matchMime` is expected to be empty string, 1/2 MIME with no trailing slash, or full MIME.
    '''
    if matchMime == '':
        return True
    else:
        targetMime = targetMime.split('/')  # Should have len = 2
        matchMime = matchMime.split('/')
        if len(targetMime) == 2:
            if len(matchMime) == 1 and matchMime[0] == targetMime[0]:
                return True
            elif len(matchMime) == 2 and matchMime[0] == targetMime[0] and matchMime[1] == targetMime[1]:
                return True
    return False


class InvalidFileNameException(Exception):
    pass
