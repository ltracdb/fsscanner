import sys
import os
import unittest
from pprint import pprint
from time import time
import logging
from unittest import TestCase
from unittest.mock import MagicMock
from fsscanner.scanner import Scanner, statify, _mimeMatch
from fsscanner.debugfilecache import DebugFileCache
from fsscanner.debugfileinterpreter import DebugFileInterpreter


logging.getLogger().setLevel(logging.DEBUG)


class test_scanner(TestCase):
    '''
    Test class scanner.Scanner. We'll try use DebugFile(Cache|Interpreter) not mock.
    '''
    test_data_dir = os.path.abspath(os.path.dirname(__file__)) + '/testdata/'
    test_no_data_dir = os.path.abspath(os.path.dirname(__file__)) + '/testnodata/'

    def setUp(self):
        cache = None

    def test_statify(self):
        ''' Test stat file. Some of this behavior is a bit idiosyncronous but .. thats not the point. '''
        statified = statify('/')
        self.assertEqual(statified['path'], '/')
        self.assertEqual(statified['name'], '')
        self.assertEqual(statified['dirname'], '/')
        self.assertEqual(statified['type'], 'directory')
        self.assertEqual(statified['mime'], 'inode/directory')
        statified = statify('/etc/passwd')
        self.assertEqual(statified['path'], '/etc/passwd')
        self.assertEqual(statified['name'], 'passwd')
        self.assertEqual(statified['dirname'], '/etc')
        self.assertEqual(statified['type'], 'file')
        self.assertEqual(statified['mime'], '')
        statified = statify('/dev/null')
        self.assertEqual(statified['path'], '/dev/null')
        self.assertEqual(statified['name'], 'null')
        self.assertEqual(statified['dirname'], '/dev')
        self.assertEqual(statified['type'], '')
        self.assertEqual(statified['mime'], '')

    def test_mimeMatch(self):
        ''' todo: test this. '''
        pass

    def test_scanner_scan(self):
        ''' Test all '''
        cache = DebugFileCache()
        interpreter = DebugFileInterpreter()
        scanner = Scanner(cache, {'': interpreter}, absolutePaths=True)
        scanner.scanDir(self.test_data_dir)
        self.assertEqual(len(cache._cache), 10)
        cache.drop()

    def test_scanner_purge_and_drop(self):
        ''' Test all '''
        cache = DebugFileCache()
        interpreter = DebugFileInterpreter()
        scanner = Scanner(cache, {'': interpreter}, absolutePaths=True)
        scanner.scanDir(self.test_data_dir)
        scanner.scanDir(self.test_no_data_dir, markUnScannedDeleted=False)
        cache.purgeDeleted()
        self.assertEqual(len(cache._cache), 11)
        scanner.scanDir(self.test_no_data_dir, markUnScannedDeleted=True)
        cache.purgeDeleted()
        self.assertEqual(len(cache._cache), 1)
        cache.drop()
        self.assertEqual(len(cache._cache), 0)

    def test_scanner_with_relative(self):
        ''' Test all '''
        cache = DebugFileCache()
        interpreter = DebugFileInterpreter()
        scanner = Scanner(cache, {'': interpreter})
        scanner.scanDir(self.test_data_dir)
        self.assertEqual(len(cache._cache), 10)
        scanner.scanDir(self.test_no_data_dir, markUnScannedDeleted=False)
        cache.purgeDeleted()
        self.assertEqual(len(cache._cache), 1)  # markUnScannedDeleted no effect without absolutePaths.
        scanner.scanDir(self.test_no_data_dir, markUnScannedDeleted=True)
        cache.purgeDeleted()
        self.assertEqual(len(cache._cache), 1)
        cache.drop()
        self.assertEqual(len(cache._cache), 0)


if __name__ == "__main__":
    unittest.main()
