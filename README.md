# Overview
This package provides the `Scanner` class in `scanner.py`:

  * `Scanner` creates a database of meta records of files in some sub tree of the local filesystem.
  * By default the record consists of: `<path, name, dirname, size, mtime, mime, meta>`
  * The scanner supports "interpreters" which can *add* arbitrary meta data to each record.
  * All such meta data is stored in a JSON encoded as a string in `meta`.
  * The backend database storage is also flexible. Scanner requires the db provide a minimal simple interface. See class `BaseFileCache`.
  * All files are uniquely identified by their `path`. See scanner.py for mor info.

# Requirements

  * python 3.4
